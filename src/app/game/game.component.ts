import { Component, OnInit } from '@angular/core';
import {PokeService} from '../service/pokeservice'

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css']
})
export class GameComponent implements OnInit {

  constructor(public service: PokeService) { }

  ngOnInit(): void {
    this.service.getRandomPokemon()
    this.service.getLocalStorage()
  }

  onEnter(){
    this.service.guessPokemon()
  }

}

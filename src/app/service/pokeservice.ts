import { Inject, Injectable } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { HttpClient } from "@angular/common/http";
import { environment } from "../../environments/environment"


@Injectable({providedIn: 'root'})

export class PokeService{
    uri = environment.api

    constructor(private http: HttpClient){}

    pForm: FormGroup = new FormGroup({
        name : new  FormControl("",Validators.required)
    })

    selectedPokemon : any
    hearts : number = 3
    played : number = 0
    wins : number = 0

    async getRandomPokemon(){
        try{
            let random = Math.floor(Math.random() * 899)
            await this.http.get(this.uri+"/"+random).subscribe((data) => {
                this.selectedPokemon = data
            }
            )
        } catch(e) {
            console.log(e)
        }
    }

    guessPokemon(){
        if (this.pForm.controls['name'].value != this.selectedPokemon.name){
            this.hearts -= 1
            console.log("Bad")
            if (this.hearts == 0 ){
                this.restartGame()
            }
        }else{
            this.wins += 1
            localStorage.setItem('wins',String(this.wins))
            this.restartGame()
        }
    }

    restartGame(){
        this.hearts = 3
        this.played += 1 
        localStorage.setItem('played',String(this.played))
        this.getRandomPokemon()
    }

    getLocalStorage(){
        this.wins = Number(localStorage.getItem('wins'))
        this.played = Number(localStorage.getItem('played'))
    }




}


